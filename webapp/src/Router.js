import Vue from 'vue'
import VueRouter from 'vue-router'

import imagePage from "./ImagePage.vue"
import uploadPage from "./UploadPage.vue"
import comparePage from "./ComparePage.vue"
import registerPage from "./RegisterPage.vue"

Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    { path: "/", component: imagePage },
    { path: "/images", component: imagePage },
    { path: "/upload", component: uploadPage },
    { path: "/compare", component: comparePage },
    { path: "/register", component: registerPage },
  ]
});

export default router;