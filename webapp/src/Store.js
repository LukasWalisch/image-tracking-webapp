import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    originalImageData: null,
    originalImageUploader: "",
  },
  mutations: {
    changeOriginalImage(state, imageData) {
      state.originalImageData = imageData;
    },
  },
});

export default store;