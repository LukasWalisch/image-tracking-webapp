import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.css'
import router from "./Router.js"
import store from "./Store.js"
import Toastr from "vue-toastr"
require('vue-toastr/src/vue-toastr.scss');

Vue.use(Vuetify)
Vue.use(Toastr)

new Vue({
  el: '#app',
  router: router,
  store: store,
  render: h => h(App)
})
