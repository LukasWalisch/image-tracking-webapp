# image-tracking-webapp
## Howto Deploy & Run

### /contract

#### Run a private Ethereum Blockchain
 The following is an example how I started a Geth node to allow a server to connect to it:
```
geth --datadir ~/devnet/node1/ --syncmode 'full' --port 30303 --rpc --rpcaddr '0.0.0.0' --rpccorsdomain '*' --rpcport 8545 --rpcapi 'db,eth,net,web3,personal,txpool,miner,admin'  --networkid 1515 --gasprice '1' --targetgaslimit 100000000 --unlock '4d3b8c2413f875518b394e8d7d2e9b27c0b8e053' --mine
```
I followed this tutorial to set up the blockchain:
https://hackernoon.com/setup-your-own-private-proof-of-authority-ethereum-network-with-geth-9a0a3750cda8

#### Deploy the smart contract
The source code for the Smart Contract is in ImageDataStore.sol
I recommend using the Solidity IDE [Remix](http://remix.ethereum.org) to deploy the Smart Contract to the blockchain. Just connect Remix to the running Geth node as Web3 Provider. Then deploy the contract and copy address and ABI from the Remix IDE.

### /backend
#### Python Dependencies 
 - python 3.6
 - flask
 - numpy
 - web3
 - opencv (for ubuntu: https://docs.opencv.org/3.4.1/d2/de6/tutorial_py_setup_in_ubuntu.html) 
 
#### Config
 - Insert ABI and address of deployed smart contract in contractdata.py
 - Insert the url of the web3 provider (Geth Node) in imagedatastore.py which is used to access the smart contract
![enter image description here](http://gdurl.com/KvZg)

#### Run
Make sure the Geth Node which is used by the server is running. Use python 3.6 environment with all required dependencies installed to run the server. 

    python restserver.py


### /webapp 

#### Dependencies
 - nodejs 8.11.3

#### Install
    npm install

#### Run
    npm run dev
starts the webapp at http://localhost:8080
   

