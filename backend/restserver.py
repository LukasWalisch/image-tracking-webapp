import os
import traceback
from threading import Timer

import time
from cv2 import cv2
from uuid import uuid4

import numpy as np

from flask import Flask, request, Response, jsonify, send_from_directory, send_file
from flask_cors import CORS
import base64

from imagefeatures import FeatureDetector, ImageData
from state import StateManager, Session

# server config
host = "http://localhost:5000"
root_dir = os.path.dirname(os.path.abspath(__file__))
image_dir = root_dir + "/images"
state_file = root_dir + "/state.pkl"

# initialize state
state_manager: StateManager = StateManager(root_dir, state_file, 5 * 60)
state_manager.load()

# start server
app = Flask(__name__)
cors = CORS(app)


@app.route("/uploadImage", methods=["POST"])
def upload_image():

    # parse image from request body
    data = request.get_json()
    image_base64 = data["image"].split(",")[1].encode("utf-8")
    image = base64.decodebytes(image_base64)

    # create session
    image_name, session_key = state_manager.add_session(image)

    # return image name and session key for future access to temporary stored image from webapp
    response_data = {"sessionKey": session_key}
    return jsonify(response_data)


@app.route("/compare", methods=["POST"])
def compare():

    # time measure
    t1 = time.time()

    # required params
    # {compareImage: base64_image,  originalImageName: str}
    data = request.get_json()

    # retrieve image_data of originalImage by originalImageName and uploader address
    original_image_name = data["originalImageName"]
    uploader_address = data["uploader"]
    orig_image_data: ImageData = state_manager.image_datastore.get_imagedata(uploader_address, original_image_name)
    if not orig_image_data:
        Response("Image data doesn't exist in smart contract", status=400)

    # parse image from request body
    compare_img_base64 = data["compareImage"].split(",")[1].encode("utf-8")
    compare_img = base64.decodebytes(compare_img_base64)

    # temporarily save image for conversion to np.ndarray
    compare_img_key = uuid4().hex
    compare_image_path = root_dir + "/images/" + compare_img_key + "_comp.jpg"
    with open(compare_image_path, "wb") as file:
        file.write(compare_img)

    # detect features of images, use same grid as for original image
    compare_feature_img, compare_image_data = FeatureDetector.detect_features(
        compare_image_path, orig_image_data.columns, orig_image_data.rows
    )

    # compare images
    compare_feature_img = FeatureDetector.compare_images(orig_image_data, compare_image_data, compare_feature_img)
    cv2.imwrite(compare_image_path, compare_feature_img)

    # delete temporary compare image after 10 seconds
    timer = Timer(10, lambda: os.remove(compare_image_path))
    timer.start()

    # time measure
    t2 = time.time()
    print("[Evaluation] comparison time: ", t2 - t1)

    return send_file(compare_image_path)


@app.route("/gridImage", methods=["GET"])
def get_grid_image():

    try:
        rows = int(request.args.get("rows"))
        columns = int(request.args.get("columns"))
    except TypeError:
        traceback.print_exc()
        return Response("invalid params", status=400)

    # validate session
    req_session_key = request.args.get("session")
    req_session: Session = None
    for session in state_manager.state.sessions:
        if session.session_key == req_session_key:
            req_session = session
    if not req_session:
        return Response("invalid session", status=400)

    # load original session image and apply modification, then send modified image as response
    t1 = time.time()
    mod_image, image_data = FeatureDetector.detect_features(req_session.original_image_path, rows, columns)
    t2 = time.time()
    print("[Evaluation] feature detection time: ", t2-t1)

    state_manager.add_modified_image(req_session, mod_image, image_data)
    return send_from_directory(root_dir + "/images", req_session.modified_image_path.split("/")[-1])


@app.route("/saveImage", methods=["POST"])
def save_image():

    data = request.get_json()

    # validate session
    req_session_key = data["session"]
    req_session: Session = None
    for session in state_manager.state.sessions:
        if session.session_key == req_session_key:
            req_session = session
    if not req_session:
        return Response("invalid session", status=400)

    image_name: str = data["imageName"]
    uploader: str = data["uploader"]
    password: str = data["password"]

    success = state_manager.save_image(req_session, image_name, uploader, password)
    if success:
        return Response(status=200)
    return Response(status=400)


# returns [ {name: urlstring, date: str, uploader: str, firstname: str, lastname: str} ]
@app.route("/imageList", methods=["GET"])
def image_list():

    # get images from image directory
    dir_image_names = [name.split(".")[0] for name in os.listdir(image_dir)]

    # get image metadata
    metadata_list = state_manager.image_datastore.get_image_metadata_list()

    # add image url to metadata list
    for image in metadata_list:
        if image["name"] in dir_image_names:

            image["image"] = host + "/image?name=" + image["name"] + ".jpg"
        else:
            image["image"] = None

    response = {"imageList": metadata_list}
    return jsonify(response)


@app.route("/test", methods=["GET"])
def test():
    filename = request.args.get("name")
    return send_from_directory(root_dir + "/images", filename)


@app.route("/image", methods=["GET", "POST"])
def route_image():
    if request.method == "POST":
        return save_image_file()
    elif request.method == "GET":
        return get_image()


# register a user in the smart contract and returns the address of the created account
# requires arguments: {firstname: str, lastname: str, password: str}
@app.route("/register", methods=["POST"])
def route_register():
    request_data = request.get_json()
    firstname = request_data["firstname"]
    lastname = request_data["lastname"]
    password = request_data["password"]
    address = state_manager.image_datastore.register_user(firstname, lastname, password)
    if address:
        return Response(address, status=200)
    return Response(status=400)


def save_image_file():
    data = request.get_json()
    image_base64 = data["image"].split(",")[1].encode("utf-8")
    with open("/home/lukas/workspace/image-tracking-backend/imageFromWebapp.jpg", "wb") as file:
        file.write(base64.decodebytes(image_base64))
    return Response("upload successful", status=200)


def get_image():
    filename = request.args.get("name")
    return send_from_directory(root_dir + "/images", filename)


if __name__ == '__main__':

    # blocking call to start server
    app.run()
