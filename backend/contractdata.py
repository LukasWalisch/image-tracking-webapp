from web3 import Web3


class ContractData:
    address = Web3.toChecksumAddress("0xa7606d5044952ccced39353531b7a0eeaffa56be")

    abi = [
        {
            "constant": False,
            "inputs": [
                {
                    "name": "_firstname",
                    "type": "string"
                },
                {
                    "name": "_lastname",
                    "type": "string"
                }
            ],
            "name": "registerUser",
            "outputs": [
                {
                    "name": "success",
                    "type": "bool"
                }
            ],
            "payable": False,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": True,
            "inputs": [
                {
                    "name": "uploader",
                    "type": "address"
                }
            ],
            "name": "getImageDataCount",
            "outputs": [
                {
                    "name": "",
                    "type": "uint256"
                }
            ],
            "payable": False,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": True,
            "inputs": [
                {
                    "name": "uploader",
                    "type": "address"
                },
                {
                    "name": "imageName",
                    "type": "string"
                }
            ],
            "name": "getImageData",
            "outputs": [
                {
                    "name": "name",
                    "type": "string"
                },
                {
                    "name": "data",
                    "type": "bytes"
                },
                {
                    "name": "date",
                    "type": "string"
                }
            ],
            "payable": False,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": True,
            "inputs": [
                {
                    "name": "",
                    "type": "uint256"
                }
            ],
            "name": "userAddresses",
            "outputs": [
                {
                    "name": "",
                    "type": "address"
                }
            ],
            "payable": False,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": True,
            "inputs": [
                {
                    "name": "userAddress",
                    "type": "address"
                }
            ],
            "name": "getUser",
            "outputs": [
                {
                    "name": "first",
                    "type": "string"
                },
                {
                    "name": "last",
                    "type": "string"
                }
            ],
            "payable": False,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": True,
            "inputs": [],
            "name": "owner",
            "outputs": [
                {
                    "name": "",
                    "type": "address"
                }
            ],
            "payable": False,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": True,
            "inputs": [
                {
                    "name": "index",
                    "type": "uint256"
                }
            ],
            "name": "getUserAddress",
            "outputs": [
                {
                    "name": "",
                    "type": "address"
                }
            ],
            "payable": False,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": True,
            "inputs": [],
            "name": "getUserCount",
            "outputs": [
                {
                    "name": "",
                    "type": "uint256"
                }
            ],
            "payable": False,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": True,
            "inputs": [
                {
                    "name": "uploader",
                    "type": "address"
                },
                {
                    "name": "index",
                    "type": "uint256"
                }
            ],
            "name": "getImageName",
            "outputs": [
                {
                    "name": "",
                    "type": "string"
                }
            ],
            "payable": False,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": True,
            "inputs": [
                {
                    "name": "userAddress",
                    "type": "address"
                }
            ],
            "name": "checkRegistered",
            "outputs": [
                {
                    "name": "success",
                    "type": "bool"
                }
            ],
            "payable": False,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": False,
            "inputs": [
                {
                    "name": "_name",
                    "type": "string"
                },
                {
                    "name": "_data",
                    "type": "bytes"
                },
                {
                    "name": "_date",
                    "type": "string"
                }
            ],
            "name": "addImageData",
            "outputs": [
                {
                    "name": "success",
                    "type": "bool"
                }
            ],
            "payable": False,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs": [],
            "payable": False,
            "stateMutability": "nonpayable",
            "type": "constructor"
        }
    ]
