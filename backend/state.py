import os
import pickle
from datetime import date

import shutil
from cv2 import cv2
from threading import Timer
from typing import List, Tuple, Optional
import numpy as np

import time
from uuid import uuid4

from imagedatastore import ImageDataStore
from imagefeatures import ImageData


class State:
    def __init__(self):
        self.sessions: List[Session] = []
        self.saved_images: List[SavedImage] = []


class Session:
    def __init__(self, session_key: str, image_name: str):
        self.session_key = session_key
        self.timestamp = time.time()
        self.original_image_path = image_name
        self.modified_image_path: str = None
        self.image_data: bytes = None


class SavedImage:
    def __init__(self, name="", uploader="", date=""):
        self.name = name
        self.uploader = uploader
        self.date = date


class StateManager:
    def __init__(self, root_path: str, state_file: str, session_expire_seconds: int):
        self.image_datastore = ImageDataStore()
        self.state: State = None
        self.state_file = state_file
        self.root_path = root_path
        self.session_expire_seconds = session_expire_seconds
        self.interval = 30
        timer = Timer(self.interval, self.invalidate_sessions)
        timer.start()

    def load(self) -> State:
        try:
            with open(self.state_file, "rb") as file:
                self.state = pickle.load(file)
                return self.state
        except FileNotFoundError:
            os.mknod(self.state_file)
            self.log("Could not find saved state, initialized new state file")
            self.state = State()
            return self.state

    def persist(self):
        with open(self.state_file, "wb") as file:
            pickle.dump(self.state, file, pickle.HIGHEST_PROTOCOL)

    # invalidate expired sessions and remove temporary images
    def invalidate_sessions(self):
        self.log("Search for expired sessions")
        now = time.time()
        for session in self.state.sessions:
            if session.timestamp + self.session_expire_seconds < now:
                try:
                    os.remove(session.original_image_path)
                    self.log("Invalidate session: " + session.session_key)
                    self.log("Remove image: " + session.original_image_path)
                except FileNotFoundError:
                    self.log("File " + session.original_image_path + "already removed")
                if session.modified_image_path:
                    try:
                        os.remove(session.modified_image_path)
                        self.log("Remove image: " + session.modified_image_path)
                    except FileNotFoundError:
                        self.log("File " + session.modified_image_path + "already removed")
                self.state.sessions.remove(session)

        self.persist()
        # function reschedules itself for periodic invocation
        timer = Timer(self.interval, self.invalidate_sessions)
        timer.start()

    def add_session(self, image) -> Tuple[str, str]:
        session_key = uuid4().hex
        image_path = self.root_path + "/images/" + session_key + ".jpg"
        with open(image_path, "wb") as file:
            file.write(image)
        session = Session(session_key, image_path)
        self.state.sessions.append(session)
        self.persist()
        return image_path, session_key

    def add_modified_image(self, session: Session, image, image_data: ImageData):
        session.modified_image_path = self.root_path + "/images/" + session.session_key + "_mod.jpg"
        cv2.imwrite(session.modified_image_path, image)
        session.image_data = image_data.toByteString()
        print("[Evaluation] image data size:", len(session.image_data))
        self.persist()

    def save_image(self, session: Session, name: str, uploader_address: str, password: str) -> bool:

        # construct the date string
        today = date.today()
        today = "{:0>2}.{:0>2}.{}".format(today.month, today.day, today.year)

        # save the image data to the smart contract and save the image on the file system
        success = self.image_datastore.add_imagedata(name, session.image_data, today, uploader_address, password)

        if not success:
            return False

        # copy the content of the temp image to the saved image
        saved_image_path = self.root_path + "/images/" + name + ".jpg"
        shutil.copyfile(session.original_image_path, saved_image_path)

        saved_image = SavedImage(name, uploader_address, today)
        self.state.saved_images.append(saved_image)

        # remove session and clean up
        os.remove(session.original_image_path)
        os.remove(session.modified_image_path)
        self.state.sessions.remove(session)
        self.persist()
        return True

    def get_saved_image(self, name:str) -> Optional[SavedImage]:
        for image in self.state.saved_images:
            image_name = image.name.split("/")[-1].split(".")[0]
            if name == image_name:
                return image
        return None


    @staticmethod
    def log(msg: str):
        print("[StateManager] " + msg)
