import pickle
from typing import List, Optional

from web3 import Web3, HTTPProvider
from contractdata import ContractData
from web3.middleware import geth_poa_middleware


# Interaction with the smart contract ImageDataStore.sol
# which is deployed on the ethereum blockchain
from imagefeatures import ImageData


class ImageDataStore:
    def __init__(self):
        self.web3 = Web3(HTTPProvider("http://131.130.122.225:8545"))
        self.web3.middleware_stack.inject(geth_poa_middleware, layer=0)
        self.contract = self.web3.eth.contract(address=ContractData.address, abi=ContractData.abi)

        self.get_image_metadata_list()


    # add image data to smart contract
    # returns success boolean value
    def add_imagedata(self, name: str, data: bytes, date: str, uploader_address: str, password: str) -> bool:
        uploader_address = Web3.toChecksumAddress(uploader_address)
        self.web3.personal.unlockAccount(uploader_address, password)
        count_before = self.contract.functions.getImageDataCount(uploader_address).call()
        print("before transaction: ", count_before)
        transaction_hash = self.contract.functions.addImageData(name, data, date).transact({"from": uploader_address, "gas": 10000000})
        self.web3.eth.waitForTransactionReceipt(transaction_hash)
        count_after = self.contract.functions.getImageDataCount(uploader_address).call()
        print("after transaction: ", count_after)
        self.web3.personal.lockAccount(uploader_address)
        if count_after == count_before + 1:
            return True
        return False

    # get image data from smart contract
    def get_imagedata(self, uploader, image_name) -> Optional[ImageData]:

        # response: [ name, data, date ]
        response = self.contract.functions.getImageData(uploader, image_name).call()
        response_name = response[0]
        response_data = response[1]
        if image_name == response_name:
            image_data: ImageData = pickle.loads(response_data)
            return image_data
        return None

    def get_imagedata_list(self) -> List[ImageData]:
        imagedata_list = []
        user_count = self.contract.functions.getUserCount().call()
        for user_idx in range(user_count):
            user_address = self.contract.functions.getUserAddress(user_idx).call()
            imagedata_count = self.contract.functions.getImageDataCount(user_address).call()
            for imagedata_idx in range(imagedata_count):
                image_name = self.contract.functions.getImageName(user_address, imagedata_idx).call()
                imagedata = self.get_imagedata(user_address, image_name)
                imagedata_list.append(imagedata)
        return imagedata_list

    # returns [ {name: str, date: str, uploader: str, firstname: str, lastname: str} ]
    def get_image_metadata_list(self):
        image_metadata_list = []
        user_count = self.contract.functions.getUserCount().call()
        for user_idx in range(user_count):
            user_address = self.contract.functions.getUserAddress(user_idx).call()
            user = self.contract.functions.getUser(user_address).call()
            imagedata_count = self.contract.functions.getImageDataCount(user_address).call()
            for imagedata_idx in range(imagedata_count):
                image_name = self.contract.functions.getImageName(user_address, imagedata_idx).call()
                image_info = self.contract.functions.getImageData(user_address, image_name).call()
                image_metadata_list.append({
                    "name": image_info[0],
                    "date": image_info[2],
                    "uploader": user_address,
                    "firstname": user[0],
                    "lastname": user[1],
                })
        return image_metadata_list

    def register_user(self, firstname: str, lastname: str, password: str) -> Optional[str]:
        new_account = self.web3.personal.newAccount(password)
        print("new user address:",new_account)

        # transfer 1 ether to new account so it can make transactions to smart contract
        transaction_hash = self.web3.eth.sendTransaction({
            "from": self.web3.eth.coinbase,
            "to": new_account,
            "value": 1000000000000000000
        })

        transaction_receipt = self.web3.eth.waitForTransactionReceipt(transaction_hash)
        self.web3.personal.unlockAccount(new_account, password)

        # register user in smart contract
        transaction_hash = self.contract.functions.registerUser(firstname, lastname).transact({
            "from": new_account,
            "gas": 10000000
        })
        self.web3.eth.waitForTransactionReceipt(transaction_hash)
        success = self.contract.functions.checkRegistered(new_account).call()
        print("creating new account successful:", success)
        self.web3.personal.lockAccount(new_account)
        if success:
            return new_account
        return None


if __name__ == "__main__":
    datastore = ImageDataStore()