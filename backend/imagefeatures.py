from typing import List, Tuple

import cv2
from cv2 import KeyPoint
import numpy as np
import util
import math

import pickle


class FeatureDetector:

    linecolor = (0, 0, 255)
    comp_linecolor = (255, 0, 0)
    textcolor = (255, 255, 255)

    # font settings depending on the image size
    font_settings = [
        {"px": 0, "scale": 0.3, "thickness": 2},
        {"px": 400, "scale": 0.4, "thickness": 2},
        {"px": 600, "scale": 0.5, "thickness": 2},
        {"px": 1000, "scale": 0.8, "thickness": 3},
        {"px": 1500, "scale": 1, "thickness": 4},
        {"px": 2000, "scale": 1.2, "thickness": 4},
        {"px": 2500, "scale": 1.5, "thickness": 5},
        {"px": 3000, "scale": 1.7, "thickness": 5},
        {"px": 3500, "scale": 1.9, "thickness": 7},
        {"px": 4000, "scale": 2, "thickness": 7},
    ]

    # detect features of image according to the given grid dimension (num columns, num rows)
    # returns image with feature information overlay and ImageData object which contains detected Features
    @staticmethod
    def detect_features(image_path: str, grid_columns, grid_rows, color=(255, 0, 0)) -> Tuple[np.ndarray, "ImageData"]:

        # load image
        image: np.ndarray = cv2.imread(image_path)
        height, width, _ = image.shape

        # calculate image sections based on given grid
        sec_height = height // grid_columns
        sec_width = width // grid_rows

        # create sections according to grid and detect features for each section
        sections: List[Section] = []
        for x in range(grid_columns):
            for y in range(grid_rows):
                start_x = x * sec_width
                start_y = y * sec_height
                patch = image[start_y:start_y + sec_height, start_x:start_x + sec_width, :]
                section = Section((start_x, start_y), patch)
                section.detectKeypoints()
                sections.append(section)

        image_data = ImageData(grid_rows, grid_columns, sec_width, sec_height, sections)

        # draw detected keypoints on image
        mod_image: np.ndarray = image[:, :, :]
        FeatureDetector.draw_keypoints(mod_image, image_data, color)
        FeatureDetector.draw_grid(mod_image, image_data)
        return mod_image, image_data

    # linewidth for grid is dependent on the image width
    # and increases with a linear function y = 1/1000 * x + 1
    @staticmethod
    def get_linewidth(imagewidth: int) -> int:
        linewidth = (1/1000) * imagewidth + 2
        return int(linewidth)

    # draws result of comparison to an image
    @staticmethod
    def draw_diff_info(image: np.ndarray, image_data: "ImageData", image_diff: List["SectionDiff"]):
        height, width, _ = image.shape
        font = cv2.FONT_HERSHEY_SIMPLEX
        fontScale = 0.5
        fontColor = (255, 0, 255)
        thickness = 2
        lineType = 1
        rel_padding = 0.15
        vert_padding = int(image_data.section_height * rel_padding)
        hor_padding = int(vert_padding/2)

        for setting in reversed(FeatureDetector.font_settings):
            if width > setting["px"]:
                print("setting: ", setting["px"])
                thickness = setting["thickness"]
                fontScale = setting["scale"]
                break

        for section, section_diff in zip(image_data.sections, image_diff):

            # draw distance info as text
            start_x, start_y = section.startPoint
            text = "d: {0:.2f}".format(section_diff.distance)
            start = (start_x + hor_padding, start_y + vert_padding)
            cv2.putText(image, text, start, font, fontScale, fontColor, thickness, lineType)

            # draw size info
            text = "s: {0:.2f}".format(section_diff.size_diff)
            start = (start_x + hor_padding, start_y + 2*vert_padding)
            cv2.putText(image, text, start, font, fontScale, fontColor, thickness, lineType)

            # draw angle info
            text = "a: {0:.2f}".format(section_diff.angle_diff)
            start = (start_x + hor_padding, start_y + 3*vert_padding)
            cv2.putText(image, text, start, font, fontScale, fontColor, thickness, lineType)

            # draw descriptors info
            text = "desc: {0:.2f}".format(section_diff.desc_distance)
            start = (start_x + hor_padding, start_y + 4*vert_padding)
            cv2.putText(image, text, start, font, fontScale, fontColor, thickness, lineType)

    @staticmethod
    def draw_grid(image: np.ndarray, image_data: "ImageData"):
        height, width, _ = image.shape
        linewidth = FeatureDetector.get_linewidth(width)

        # draw vertical grid lines
        for i in range(1, image_data.columns):
            start = (i * image_data.section_width, 0)
            end = (i * image_data.section_width, height)
            cv2.line(image, start, end, FeatureDetector.linecolor, linewidth)

        # draw horizontal gridlines
        for i in range(1, image_data.rows):
            start = (0, i * image_data.section_height)
            end = (width, i * image_data.section_height)
            cv2.line(image, start, end, FeatureDetector.linecolor, linewidth)

    # draws keypoints from image_data to image
    # the keypoints are transformed from their location in the section to their location in the image
    @staticmethod
    def draw_keypoints(image: np.ndarray, image_data: "ImageData", color=(0, 0, 255)):
        height, width, _ = image.shape
        linewidth = FeatureDetector.get_linewidth(width)
        for section in image_data.sections:
            offset_x, offset_y = section.startPoint
            for kp in section.keypoints:
                center_x = int(kp.pt[0] + offset_x)
                center_y = int(kp.pt[1] + offset_y)
                cv2.circle(image, (center_x, center_y), int(kp.size), color, linewidth)
                line_end_x = int(kp.size * math.cos(kp.angle) + center_x)
                line_end_y = int(kp.size * math.sin(kp.angle) + center_y)
                cv2.line(image, (center_x, center_y), (line_end_x, line_end_y),color, linewidth)

    @staticmethod
    def compare_images(original_image_data: "ImageData", modified_image_data: "ImageData", mod_image: np.ndarray) -> np.ndarray:
        orig_sections = original_image_data.sections
        mod_sections = modified_image_data.sections
        matcher = cv2.BFMatcher()

        # calculate difference values for each section
        # save as list of SectionDiff elements
        image_diff: List[SectionDiff] = []
        for i, (mod_section, orig_section) in enumerate(zip(mod_sections, orig_sections)):
            distance = 0
            angle_diff = 0
            size_diff = 0

            # find keypoint matches
            # TODO: if mod_section has more keypoints than orig_section,
            # TODO: this difference should be expressed. atm it is ignored
            if not orig_section.descriptors.any() or not mod_section.descriptors.any():
                section_diff = SectionDiff(0, 0, 0, 0)
            else:
                matches = matcher.match(mod_section.descriptors, orig_section.descriptors)
                desc_distance = 0
                keypoint_matches: List[Tuple[KeyPoint, KeyPoint]] = []
                for match in matches:

                    # sum up descriptor distance values
                    desc_distance += match.distance

                    # sort keypoints according to matches
                    keypoint_matches.append(
                        (mod_section.keypoints[match.queryIdx], orig_section.keypoints[match.trainIdx])
                    )

                # compare keypoints in section
                for mod_keypoint, orig_keypoint in keypoint_matches:
                    orig_x, orig_y = orig_keypoint.pt
                    mod_x, mod_y = mod_keypoint.pt

                    # calculate euclidean distance
                    dist = math.sqrt(math.pow(mod_x - orig_x, 2) + math.pow(mod_y - orig_y, 2))

                    # calculate angle and size difference
                    angle = abs(mod_keypoint.angle - orig_keypoint.angle)
                    size = abs(mod_keypoint.size - orig_keypoint.size)

                    distance += dist
                    angle_diff += angle
                    size_diff += size
                section_diff = SectionDiff(distance, angle_diff, size_diff, desc_distance)
            image_diff.append(section_diff)

        # draw image_diff information as overlay to mod_image

        # draw features of original images in blue for comparison
        FeatureDetector.draw_keypoints(mod_image, original_image_data, (0, 0, 255))
        FeatureDetector.draw_diff_info(mod_image, modified_image_data, image_diff)
        return mod_image


# image section
class Section:
    def __init__(self, startPoint=None, imagePatch=None):
        self.startPoint = startPoint
        self.imagePatch = imagePatch
        self.height, self.width, _ = self.imagePatch.shape
        self.keypoints: List[cv2.KeyPoint] = None
        self.descriptors: np.ndarray = None

    def detectKeypoints(self):

        # using SURF algorithm to detect keypoints and BRIEF algorithm to compute descriptors
        surf = cv2.xfeatures2d.SURF_create()
        keypoints = surf.detect(self.imagePatch)
        brief = cv2.xfeatures2d.BriefDescriptorExtractor_create(bytes=16)
        keypoints, descriptors = brief.compute(self.imagePatch, keypoints)
        if not keypoints:
            self.keypoints = []
            self.descriptors = np.zeros(1)
            return

        descriptorList = np.split(descriptors, descriptors.shape[0])

        kpDescPairs = [{ "kp": keypoint, "desc": descriptor } for keypoint, descriptor in zip(keypoints, descriptorList)]
        kpDescPairs = sorted(kpDescPairs, key=lambda kpDescPair: -kpDescPair["kp"].response)[:5]
        keypoints = []
        descriptorList = []
        for pair in kpDescPairs:
            keypoints.append(pair["kp"])
            descriptorList.append(pair["desc"])
        self.keypoints = keypoints
        self.descriptors = np.vstack(descriptorList)

    # creates section data objects from the current section
    def exportData(self) -> "SectionData":
        return SectionData(self.startPoint, self.width, self.height, self.keypoints, self.descriptors)


# data object that can serialized for storing on blockchain
# contains all information about important features of a section
class SectionData:
    def __init__(self, startPoint, width, height, keypoints, descriptors):
        self.startPoint = startPoint
        self.width = width
        self.height = height
        self.keypoints = keypoints
        self.descriptors = descriptors


# image data which contains
# all data which is extracted and persisted for later comparison with an image
#
class ImageData:
    def __init__(self, rows: int, columns: int, sectionWidth: int, sectionHeight: int, sections: List[Section]):
        self.rows = rows
        self.columns = columns
        self.section_width = sectionWidth
        self.section_height = sectionHeight
        self.sections = sections

    def persist(self, path):

        # convert keypoints to PickleKeypoints for pickling
        pickleSections = []
        for section in self.sections:
            pickleSection: SectionData = section.exportData()
            pickleSection.keypoints = []
            for kp in section.keypoints:
                pickleSection.keypoints.append(PickleKeypoint(kp))
            pickleSections.append(pickleSection)

        self.sections = pickleSections
        util.save_obj(self, path)

    @staticmethod
    def load(path) -> "ImageData":
        imageData: "ImageData" = util.load_obj(path)

        # convert PickleKeypoints to keypoints
        for section in imageData.sections:
            section.keypoints = [ pickleKeypoint.getKeypoint() for pickleKeypoint in section.keypoints ]
        return imageData

    def toByteString(self) -> bytes:
        # convert keypoints to PickleKeypoints for pickling
        pickleSections = []
        for section in self.sections:
            pickleSection: SectionData = section.exportData()
            pickleSection.keypoints = []
            for kp in section.keypoints:
                pickleSection.keypoints.append(PickleKeypoint(kp))
            pickleSections.append(pickleSection)

        self.sections = pickleSections
        return pickle.dumps(self, pickle.HIGHEST_PROTOCOL)

    @staticmethod
    def fromByteString(bytestring: bytes) -> "ImageData":
        image_data: "ImageData" = pickle.loads(bytestring)
        return image_data

# class is needed because the cv2.KeyPoint class can't be serialized with pickle
class PickleKeypoint:
    def __init__(self, keypoint: KeyPoint):
        self.response = keypoint.response
        self.pt = keypoint.pt
        self.size = keypoint.size
        self.angle = keypoint.angle
        self.octave = keypoint.octave

    def getKeypoint(self) -> KeyPoint:
        keypoint = KeyPoint(self.pt[0], self.pt[1], self.size, self.angle, self.response, self.octave)
        return keypoint


class SectionDiff:
    def __init__(self, distance, angle_diff, size_diff, desc_distance):
        self.distance = distance
        self.angle_diff = angle_diff
        self.size_diff = size_diff
        self.desc_distance = desc_distance

