from typing import Tuple

import cv2
import pickle
import numpy


def showImage(image):
    cv2.imshow('image', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def save_obj(obj, name):
    with open(name, 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


def load_obj(name):
    with open(name, 'rb') as f:
        return pickle.load(f)

# creates an image with grid overlay for given image and grid (num columns, num rows)
def draw_grid(image: numpy.array, grid_columns, grid_rows):

    # draw vertical lines
    for i in range(1, grid_columns):
        startPoint = (i * self.secWidth, 0)
        endPoint = (i * self.secWidth, self.height)
        cv2.line(image, startPoint, endPoint, self.lineColor, self.lineWidth)

    # draw horizontal lines
    for i in range(1, self.rows):
        startPoint = (0, i * self.secHeight)
        endPoint = (self.width, i * self.secHeight)
        cv2.line(image, startPoint, endPoint, self.lineColor, self.lineWidth)
    return image