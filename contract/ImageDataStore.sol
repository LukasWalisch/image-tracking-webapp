pragma solidity ^0.4.0;

contract ImageDataStore {
    
    function ImageDataStore() public {
        owner = msg.sender;
    }

    address public owner;
  
    mapping (address => User) users; 
    address[] public userAddresses;
  
    struct User {
        string firstname;
        string lastname;
        
        // image data mapped by image name
        mapping(string => ImageData) imageDataEntries;
          
        // indexes for imageData names
        string[] imageNames;
        
        bool isInitialized;
    }   
  
    struct ImageData {
        string name;
        bytes data;
        string date;
        
        bool isInitialized;
    }
    
    function getImageData(address uploader, string imageName) public constant returns (string name, bytes data, string date) {
        User storage user = users[uploader];
        if (user.isInitialized) {
            ImageData storage imageData = user.imageDataEntries[imageName];
            if (imageData.isInitialized) {
                return (imageData.name, imageData.data, imageData.date);
            }
        }
        return ("", "", "");
    }
    
    function getImageName(address uploader, uint index) public constant returns (string) {
        User storage user = users[uploader];
        if (user.isInitialized && index < user.imageNames.length ) {
            return user.imageNames[index];
        }
        return "";
    }
    
    function getImageDataCount(address uploader) public constant returns (uint) {
        User storage user = users[uploader];
        if (user.isInitialized) {
            return user.imageNames.length; 
        }
        return 0;
    }
    
    function getUser(address userAddress) public constant returns (string first, string last) {
        User storage user = users[userAddress];
        if (user.isInitialized) {
            return (user.firstname, user.lastname);
        }
        return ("Lukas", "Walisch");
    }
    
    function getUserAddress(uint index) public constant returns (address) {
        if (index < userAddresses.length) {
            return userAddresses[index];
        }
        return address(0);
    }
    
    function getUserCount() public constant returns (uint) {
        return userAddresses.length;
    }
    
    function registerUser(string _firstname, string _lastname) public returns (bool success) {
        
        User storage user = users[msg.sender];
        if (user.isInitialized) {
            return false;
        }
        
        user.firstname = _firstname;
        user.lastname = _lastname;
        user.isInitialized = true;
        
        userAddresses.push(msg.sender);
        return true;
    }
    
    function addImageData(string _name, bytes _data, string _date) public returns (bool success) {
        User storage user = users[msg.sender];
        ImageData storage imageData = user.imageDataEntries[_name];
        
        // make sure user exists and there is no image with the same name
        if (user.isInitialized && !imageData.isInitialized) {
            imageData.name = _name;
            imageData.data = _data;
            imageData.date = _date;
            user.imageNames.push(_name);
            imageData.isInitialized = true;
            return true;
        }
        return false;
    }
    
    function checkRegistered(address userAddress) public constant returns (bool success) {
        User storage user = users[userAddress];
        if (user.isInitialized) {
            return true;
        }
        return false;
    }
    
}